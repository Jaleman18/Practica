﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            int E = 0;
            Console.WriteLine("***Escriba Una Opción Del 1 Al 3 Para Empezar***");
            E = Int32.Parse(Console.ReadLine());
            switch (E)
            {
                case 1:
                    ProgramEstructu.usandoWhile();
                    break;

                case 2:
                    ProgramEstructu.usandoForsAnidados();
                    break;
                case 3:
                    ProgramEstructu.usandoDoWhile();
                    break;
                default:
                    Console.WriteLine("Opción No Valida");
                    break;
            }
            Console.ReadLine();
        }

    }

}


