﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class ProgramEstructu
    {
        public static void usandoWhile()
        {
            int a, b;
            Console.WriteLine(" Ya Estamos Dentro Del Metodo Vamos a Usar Un While \n ");
            Console.WriteLine("---Escribe un numero principal--- \n");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine(" \n*Mientras Que Escribas Un Numero Mayor O Igual Al  Anterior, Podras Seguir Escribiendo*");
            Console.WriteLine("Escribir Un Numero");
            b = int.Parse(Console.ReadLine());
            while (b >= a)
            {
                Console.WriteLine("\nPuede Escribir Otro Número");
                b = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("\n**Ingreso Un Numero Menor Al Primero**");

        }

        public static void usandoForsAnidados()
        {
            int tabla, numero, b;
            Console.WriteLine("****Vamos A Ver Las Tablas De Multiplicar De Ciertos Numeros****\n");
            Console.WriteLine("---Coloque 1 numero del 2-10 para ver su tabla de multiplicar---");
            b = Int32.Parse(Console.ReadLine());
            switch (b)
            {
                case 2:
                    Console.WriteLine(" Lo siguiente es la tabla de multiplicar del 2 es la siguiente: \n ");
                    for (tabla = 2; tabla <= 2; tabla++)
                    {
                        for (numero = 0; numero <= 10; numero++)
                        {
                            Console.WriteLine( tabla + " * " + numero + " es: " + tabla * numero);
                        }
                    }
                    break;
                case 3:
                    Console.WriteLine(" Lo siguiente es la tabla de multiplicar del 3 es la siguiente: \n ");
                    for (tabla = 3; tabla <= 3; tabla++)
                    {
                        for (numero = 0; numero <= 10; numero++)
                        {
                            Console.WriteLine( tabla + " * " + numero + " es: " + tabla * numero);
                        }
                    }
                    break;
                case 4:
                    Console.WriteLine(" Lo siguiente es la tabla de multiplicar del 4 es la siguiente: \n ");
                    for (tabla = 4; tabla <= 4; tabla++)
                    {
                        for (numero = 0; numero <= 10; numero++)
                        {
                            Console.WriteLine( tabla + " * " + numero + " es: " + tabla * numero);
                        }
                    }
                    break;
                case 5:
                    Console.WriteLine(" Lo siguiente es la tabla de multiplicar del 5 es la siguiente: \n ");
                    for (tabla = 5; tabla <= 5; tabla++)
                    {
                        for (numero = 0; numero <= 10; numero++)
                        {
                            Console.WriteLine( tabla + " * " + numero + " es: " + tabla * numero);
                        }
                    }
                    break;
                case 6:
                    Console.WriteLine(" Lo siguiente es la tabla de multiplicar del 6 es la siguiente: \n ");
                    for (tabla = 6; tabla <= 6; tabla++)
                    {
                        for (numero = 0; numero <= 10; numero++)
                        {
                            Console.WriteLine( tabla + " * " + numero + " es: " + tabla * numero);
                        }
                    }
                    break;
                case 7:
                    Console.WriteLine(" Lo siguiente es la tabla de multiplicar del 7 es la siguiente: \n ");
                    for (tabla = 7; tabla <= 7; tabla++)
                    {
                        for (numero = 0; numero <= 10; numero++)
                        {
                            Console.WriteLine( tabla + " * " + numero + " es: " + tabla * numero);
                        }
                    }
                    break;
                case 8:
                    Console.WriteLine(" Lo siguiente es la tabla de multiplicar del 8 es la siguiente: \n ");
                    for (tabla = 8; tabla <= 8; tabla++)
                    {
                        for (numero = 0; numero <= 10; numero++)
                        {
                            Console.WriteLine( tabla + " * " + numero + " es: " + tabla * numero);
                        }
                    }
                    break;
                case 9:
                    Console.WriteLine(" Lo siguiente es la tabla de multiplicar del 9 es la siguiente: \n ");
                    for (tabla = 9; tabla <= 9; tabla++)
                    {
                        for (numero = 0; numero <= 10; numero++)
                        {
                            Console.WriteLine( tabla + " * " + numero + " es: " + tabla * numero);
                        }
                    }
                    break;
                case 10:
                    Console.WriteLine(" Lo siguiente es la tabla de multiplicar del 10 es la siguiente: \n ");
                    for (tabla = 10; tabla <= 10; tabla++)
                    {
                        for (numero = 0; numero <= 10; numero++)
                        {
                            Console.WriteLine( tabla + " * " + numero + " es: " + tabla * numero);
                        }
                    }
                    break;
            }
        }
        public static void usandoDoWhile()
        {
            string valida;
            string Usuario = "1024";
            string Contraseña = "4567";
            Console.WriteLine(" Ya Estamos Dentro Del Metodo, Vamos a Usar Un Do While \n ");

            do
            {
                Console.WriteLine("---Introduza Su Usuario---\n");
                valida = Console.ReadLine();
                if (Usuario != valida)
                {
                    Console.WriteLine(" El Usuario Introducido No Es El Correcta, Por Favor Vuelve A Intentarlo \n ");
                }

            } while (Usuario != valida);
            Console.WriteLine("***El Usuario Que Introduciste Es Correcto***");

            do
            {
                Console.WriteLine("---Introduzca La Contraseña Del Usuario---\n");
                valida = Console.ReadLine();
                if (Contraseña != valida)
                {
                    Console.WriteLine(" La contraseña no es la correcta, por favor vuelve a intentarlo \n ");
                }
            } while (Contraseña != valida);
            Console.WriteLine("La Contraseña Que Introduciste Es Correcta \n ");
            Console.WriteLine("Todo Listo Para Empezar");
        }

    }
}
        